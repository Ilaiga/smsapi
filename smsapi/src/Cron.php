<?php
/**
 * Created by PhpStorm.
 * User: duknuken666
 * Date: 19/05/2019
 * Time: 02:35
 */

namespace App;

class Cron
{
	private $db = null;
	private $mongo;

	public function __construct($db, $mongo)
	{
		$this->db = $db;
		$this->mongo = $mongo;

	}
	public function send() {

		$sth = $this->db->prepare("SELECT * FROM `cron_message` WHERE cron_status = 'NEW' ORDER BY cron_priority ASC");
		$sth->execute();
		$result = $sth->fetchObject();

		if(!$result)
			return true;

		$class = new $result->cron_class($this->db, $this->mongo);
		$data = unserialize($result->cron_object);
		$cron_id = $result->cron_id;

		$sth = $this->db->prepare("UPDATE `cron_message` SET cron_status = 'SENDING' WHERE cron_id = :cron_id");
		$sth->bindParam("cron_id", $cron_id);
		$sth->execute();

		$class->set($data['phone'], $data['sender'], $data['message'], 1);
		$class->send_();

		$sth = $this->db->prepare("UPDATE `cron_message` SET cron_status = 'YES' WHERE cron_id = :cron_id");
		$sth->bindParam("cron_id", $cron_id);
		$sth->execute();
	}

}