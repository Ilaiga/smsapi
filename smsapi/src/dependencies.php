<?php

use Slim\App;

return function (App $app) {
    $container = $app->getContainer();

    // view renderer
    $container['renderer'] = function ($c) {
        $settings = $c->get('settings')['renderer'];
        return new \Slim\Views\PhpRenderer($settings['template_path']);
    };

    // monolog
    $container['logger'] = function ($c) {
        $settings = $c->get('settings')['logger'];
        $logger = new \Monolog\Logger($settings['name']);
        $logger->pushProcessor(new \Monolog\Processor\UidProcessor());
        $logger->pushHandler(new \Monolog\Handler\StreamHandler($settings['path'], $settings['level']));
        return $logger;
    };

    $container['mongo'] = function($c) {
	    $settings = $c->get('settings')['mongo'];
	    return (new MongoDB\Client('mongodb://'.$settings['host']))->{$settings['db']};
    };

    $container['validator'] = function () {
        return new Awurth\SlimValidation\Validator();
    };

    $container['SendMessage'] = function ($c) {
        return new \App\api\SendMessage($c);
    };

    $container['GetStatus'] = function ($c) {
        return new \App\api\GetStatus($c);
    };

	$container['GetReport'] = function ($c) {
		return new \App\api\GetReport($c);
	};

    $container['phpErrorHandler'] = function () {
        return function ($request, $response, $error) {
            return $response->withStatus(500)
                ->withJson([
                    'error' => 'bad_request',
                    'error_description' => 'Error: ' . $error->getMessage()
                ]);
        };
    };

    $container['notFoundHandler'] = function () {
        return function ($request, $response) {
            return $response->withStatus(404)
                ->withJson([
                    'error' => 'not_found',
                    'error_description' => 'Method not found'
                ]);
        };
    };

    $container['errorHandler'] = function () {
        return function ($request, $response, $exception) {
            return $response->withStatus(500)
                ->withJson([
                    'error' => 'bad_request',
                    'error_description' => 'Error: ' . $exception->getMessage()
                ]);
        };
    };

    $container['notAllowedHandler'] = function () {
        return function ($request, $response) {
            return $response->withStatus(405)
                ->withJson([
                    'error' => 'not_allowed',
                    'error_description' => 'Method not allowed'
                ]);
        };
    };

    //db
	$container['DbConnection'] = function ($c) {
		$settings = $c->get('settings')['db'];
        $host = $settings['host'];
        $dbname = $settings['dbname'];
        $charset = 'utf8';
        $collate = 'utf8_unicode_ci';
        $dsn = "mysql:host=$host;dbname=$dbname;charset=$charset";
        $options = [
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_PERSISTENT => false,
            PDO::ATTR_EMULATE_PREPARES => false,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES $charset COLLATE $collate"
        ];
        return new PDO($dsn, $settings['user'], $settings['pass'], $options);
	};

	$container['Cron'] = function ($c) {
		return new \App\Cron($c['DbConnection'], $c['mongo']);
	};

	//WhatsApp
	$container['SendWhatsApp'] = function ($c) {
		return new \App\adapters\SendWhatsApp($c['DbConnection'], $c['mongo']);
	};

	//SendPulse
	$container['SendPulse'] = function ($c) {
		return new \App\adapters\SendPulse($c['DbConnection'], $c['mongo']);
	};

};
