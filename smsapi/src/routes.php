<?php

use Slim\App;
use Slim\Http\Request;
use Slim\Http\Response;

return function (App $app) {
    $container = $app->getContainer();

    $app->get('/api/v1/sendMessage', function (Request $request, Response $response, array $args) use ($container) {
        return $response->withJson($this->get('SendMessage')->run($request));
    });

    $app->get('/api/v1/getStatus', function (Request $request, Response $response, array $args) use ($container) {
        return $response->withJson($this->get('GetStatus')->run($request));
    });

	$app->get('/api/v1/getReport', function (Request $request, Response $response, array $args) use ($container) {
		return $response->withJson($this->get('GetReport')->run($request));
	});

	$app->get('/api/v1/cron', function (Request $request, Response $response, array $args) use ($container) {

		$cron = $this->get('Cron');
		$cron->send();
	});
};
