<?php

use Slim\App;

return function (App $app) {
    $app->add(new Tuupola\Middleware\HttpBasicAuthentication([
        "users" => [
            "user" => "user",
        ],
        "secure" => false,
    ]));
};
