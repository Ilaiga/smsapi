<?php
/**
 * Created by PhpStorm.
 * User: duknuken666
 * Date: 18/05/2019
 * Time: 19:15
 */


namespace App;

class Db
{

	private $db;
	public function __construct($db)
	{
		$this->db = $db;
	}
	public function getInstance() {
		return $this->db;
	}

}