<?php
/**
 * Created by PhpStorm.
 * User: duknuken666
 * Date: 18/05/2019
 * Time: 16:59
 */
namespace App\interfaces;

interface Adapters
{
	public function __construct(array $numbers, string $sender, string $message);
	public function send();
	public function status(int $statusId);
}