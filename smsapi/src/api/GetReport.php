<?php
/**
 * Created by PhpStorm.
 * User: Vika
 * Date: 19.05.2019
 * Time: 1:45
 */

namespace App\api;

use Slim\Http\Request;
use Respect\Validation\Validator as V;

class GetReport extends BaseMethod
{
    public function run(Request $request)
    {
        $params = $this->validate($request);
        $mongo = $this->container->mongo->stats;
        $cursor = $mongo->find([
        	'date' => [
        		'$gte' => $params['dateFrom'],
        		'$lt' => $params['dateTo'],
	        ]
        ]);
        //todo streaming json
	    foreach ($cursor as $document) {
		    $result[] = $document;
	    }
        return $result ?? [];
    }

    protected function rules()
    {
        return [
            'dateFrom' => V::notBlank(),
            'dateTo' => V::notBlank(),
        ];
    }
}