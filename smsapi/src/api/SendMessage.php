<?php
/**
 * Created by PhpStorm.
 * User: Vika
 * Date: 18.05.2019
 * Time: 16:07
 */
namespace App\api;

use Slim\Http\Request;
use Respect\Validation\Validator as V;

class SendMessage extends BaseMethod
{
    public function run(Request $request)
    {
        $params = $this->validate($request);
        if(empty($this->providers[$params['provider']])) throw new \Exception('Provider not exist');
        $adapter = $this->container->{$this->providers[$params['provider']]};
        $adapter->set($params['phones'], $params['sign'], $params['message'], $params['priority']);
        $result = $adapter->send();
        return ['status' => 'success', 'sendId' => $result];
    }

    public function rules()
    {
        return [
            'sign' => V::notBlank()->length(1,30),
            'phones' => V::notBlank()->arrayVal(),
            'message' => V::notBlank()->length(1, 400),
            'provider' => V::notBlank()->length(3, 255),
            'priority' => V::notBlank()->between(1,10),
        ];
    }

}