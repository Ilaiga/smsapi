<?php
/**
 * Created by PhpStorm.
 * User: Vika
 * Date: 19.05.2019
 * Time: 0:47
 */

namespace App\api;

use Slim\Http\Request;
use Respect\Validation\Validator as V;

class GetStatus extends BaseMethod
{
    public function run(Request $request)
    {
        $params = $this->validate($request);
        if(empty($this->providers[$params['provider']])) throw new \Exception('Provider not exist');
        $adapter = $this->container->{$this->providers[$params['provider']]};
        $status = $adapter->status($params['sendId']);
        return ['status' => $status];
    }

    public function rules()
    {
        return [
            'sendId' => V::notBlank()->length(1,10),
            'provider' => V::notBlank()->length(3, 255),
        ];
    }
}