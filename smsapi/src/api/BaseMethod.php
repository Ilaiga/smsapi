<?php
/**
 * Created by PhpStorm.
 * User: Vika
 * Date: 19.05.2019
 * Time: 0:48
 */

namespace App\api;

use Respect\Validation\Exceptions\ValidationException;
use Slim\Http\Request;
use Respect\Validation\Validator as V;

abstract class BaseMethod
{
    protected $container;
    protected $providers = [
        'pulse' => 'SendPulse',
	    'whatsapp' => 'SendWhatsApp'
    ];

    public function __construct($container)
    {
        $this->container = $container;
    }

    protected function validate(Request $request)
    {
        $data = $request->getQueryParam('data');
        $params = json_decode($data, true);
        $validator = $this->container->validator->array($params, $this->rules());
        if (!$validator->isValid()) {
            $errors = $validator->getErrors();
            $msg = "";
            foreach ($errors as $error) {
                $msg .= implode('. ', $error);
            }
            throw new ValidationException($msg);
        } else {
            return $params;
        }
    }

    abstract protected function run(Request $request);
    abstract protected function rules();

}