<?php
/**
 * Created by PhpStorm.
 * User: duknuken666
 * Date: 18/05/2019
 * Time: 15:11
 */
namespace App\adapters;

use App\Db;
use App\interfaces\Adapters;
use Slim\App;
use Slim\Http\Request;
use Slim\Http\Response;
use \GuzzleHttp\Client;

use Sendpulse\RestApi\ApiClient;
use Sendpulse\RestApi\Storage\FileStorage;

class SendPulse extends RecordsSmsLists
{
	const API_USER_ID = '26ba1b3acefadc27ab486d8a76c4b157';
	const API_SECRET = 'f1dbaa080c3f2d0cadd27c9227146f63';

	private $phones = [];
	private $sender = null;
	private $message = null;
	private $priority = null;
	protected $apiClient = null;

	public function set(array $numbers, string $sender, string $message, int $priority) {
		$this->phones = $numbers;
		$this->apiClient = new ApiClient(self::API_USER_ID, self::API_SECRET, new FileStorage());
		$this->sender = $sender;
		$this->message = $message;
		$this->priority = $priority;
	}

	public function send() {
		return $this->createCronTask([
			'phone' => $this->phones,
			'sender' => $this->sender,
			'message' => $this->message
		],$this->priority, self::class);
	}

	public function send_() {
		$params = [
			'sender' => $this->sender,
			'body' => $this->message,
		];
		$additionalParams = [
			'transliterate' => 0
		];

		//$this->apiClient->sendSmsByList($this->phones, $params, $additionalParams);
		$result = $this->apiClient->sendSmsByList($this->phones, $params, $additionalParams);
		$result = json_decode($result);
		return $this->saveStatus($result, self::class);
	}

	public function status(int $statusId)
	{
		return $this->getStatus($statusId);
	}
}