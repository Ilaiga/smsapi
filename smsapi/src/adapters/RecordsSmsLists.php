<?php
/**
 * Created by PhpStorm.
 * User: duknuken666
 * Date: 18/05/2019
 * Time: 17:24
 */

namespace App\adapters;

abstract class RecordsSmsLists //implements Adapters
{
	private $table = 'adapter';
	private $db = null;
	private $mongo;


	// NEW , SENDING , YES

	public function saveStatus(array $params, string $type) {
		$sth = $this->db->prepare("INSERT INTO ".$this->table." (adapter_params,adapter_type) VALUES (:params, :type)");
		$sth->bindParam("params", json_encode($params));
		$sth->bindParam("type", $type);
		$sth->execute();
		$id = $this->db->lastInsertId();
		return $id;
	}
	public function getParams(int $status_id) {
		$sth = $this->db->prepare("SELECT * FROM ".$this->table." WHERE adapter_id=:status LIMIT 1");
		$sth->bindParam("status", $status_id);
		$sth->execute();
		return $sth->fetchObject();
	}
	public function getStatus(int $status_id) {
		$result = json_decode($this->getParams($status_id)->adapter_params);
		$result = current($result)->result;
		return $result ? 'Отправлено' : 'Не отправлено';
	}

	// CRON TABLES

	public function createCronTask(array $data, int $priority, string $class) {

		$sth = $this->db->prepare("INSERT INTO `cron_message` (cron_object, cron_status, cron_priority, cron_class) VALUE (:object, 'NEW', :priority, :class)");
		$sth->bindParam("object", serialize($data));
		$sth->bindParam("priority", $priority);
		$sth->bindParam("class", $class);
		$sth->execute();
		$id = $this->db->lastInsertId();
		$this->mongo->stats->insertOne([
			'sendId' => $id,
			'sign' => $data['sender'],
			'message' => $data['message'],
			'phones' => $data['phone'],
			'date' => date('Y-m-d H:i:s')
		]);
		return $id;
	}

	public function __construct($db, $mongo)
	{
		$this->db = $db;
		$this->mongo = $mongo;
	}
}