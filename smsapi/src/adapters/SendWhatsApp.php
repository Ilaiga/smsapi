<?php
/**
 * Created by PhpStorm.
 * User: duknuken666
 * Date: 18/05/2019
 * Time: 23:28
 */

namespace App\adapters;


use GuzzleHttp\Psr7\Request;

class SendWhatsApp extends RecordsSmsLists
{
	const API_TOKEN = 'hcwsc06grlcd6kxz';
	const API_URL = 'https://eu18.chat-api.com/instance41858/';

	private $phones = [];
	private $sender = null;
	private $message = null;
	private $priority = null;

	public function set(array $numbers, string $sender, string $message, int $priority) {
		$this->phones = $numbers;
		$this->sender = $sender;
		$this->message = $message;
		$this->priority = $priority;
	}

	public function send() {
		return $this->createCronTask([
			'phone' => $this->phones,
			'sender' => $this->sender,
			'message' => $this->message
		],$this->priority, self::class);
	}

	public function send_() {
		$params = [
			'phones' => $this->phones,
			'groupName' => $this->sender,
			'messageText' => $this->message
		];
		$json = json_encode($params);
		$url = self::API_URL.'/group?token='.self::API_TOKEN;
		$curl = curl_init($url);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($curl, CURLOPT_POSTFIELDS, $json);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array(
			'Content-Type: application/json',
			'Content-Length: ' . strlen($json))
		);
		$output = curl_exec($curl);
		curl_close($curl);
		return $this->saveStatus((array)json_decode($output),self::class);
	}
	public function getStatus(int $status_id)
	{
		//$result = $this->getParams($status_id);
		//var_dump($result);
		$result = json_decode($this->getParams($status_id)->adapter_params);
		$result = $result->created;
		return $result ? 'Отправлено' : 'Не отправлено';
	}

	public function status(int $statusId) {
		return $this->getStatus($statusId);
	}


}