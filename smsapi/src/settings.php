<?php
return [
    'settings' => [

		"db" => [
			"host" => "localhost",
			"dbname" => "smsapi",
			"user" => "admin_hotelsi",
			"pass" => "b7tMsBoee7"
		],
	    "mongo" => [
		    "host" => "localhost",
		    "db" => "smsapi",
	    ],
        'displayErrorDetails' => true, // set to false in production
        'addContentLengthHeader' => false, // Allow the web server to send the content-length header

        // Renderer settings
        'renderer' => [
            'template_path' => __DIR__ . '/../templates/',
        ],

        // Monolog settings
        'logger' => [
            'name' => 'slim-app',
            'path' => isset($_ENV['docker']) ? 'php://stdout' : __DIR__ . '/../logs/app.log',
            'level' => \Monolog\Logger::DEBUG,
        ],


    ],
];
